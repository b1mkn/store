<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropOrdersEntity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('order_items');
        Schema::drop('orders');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email');
            $table->string('pricingRule');
            $table->string('pricingRuleParams');
            $table->timestamps();
        });

        Schema::create('order_items', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('orderId');
            $table->integer('productId');
            $table->double('quantity');

            $table->foreign('orderId', 'order_items_orderId_foreign')->references('id')->on('orders')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('orderId', 'order_items_productId_foreign')->references('id')->on('products')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }
}
