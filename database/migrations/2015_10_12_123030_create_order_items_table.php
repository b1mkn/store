<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('orderId');
            $table->integer('productId');
            $table->double('quantity');

            $table->foreign('orderId', 'order_items_orderId_foreign')->references('id')->on('orders')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('orderId', 'order_items_productId_foreign')->references('id')->on('products')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function(Blueprint $table) {
            $table->dropForeign('order_items_orderId_foreign');
            $table->dropForeign('order_items_productId_foreign');
        });

        Schema::drop('order_items');
    }
}
