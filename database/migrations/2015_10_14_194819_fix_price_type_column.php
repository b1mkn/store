<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Product;

class FixPriceTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('priceType');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->enum('priceType', Product::getPriceTypeList());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('priceType');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->decimal('priceType');
        });
    }
}
