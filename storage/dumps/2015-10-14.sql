--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: homestead; Tablespace: 
--

CREATE TABLE migrations (
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO homestead;

--
-- Name: pricing_rules; Type: TABLE; Schema: public; Owner: homestead; Tablespace: 
--

CREATE TABLE pricing_rules (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    CONSTRAINT pricing_rules_name_check CHECK (((name)::text = ANY ((ARRAY['App\Models\PricingRule\CustomPriceAfterCustomQuantity'::character varying, 'App\Models\PricingRule\CustomPriceForCustomQuantity'::character varying, 'App\Models\PricingRule\FreeItemAfterRequiredQuantity'::character varying])::text[])))
);


ALTER TABLE pricing_rules OWNER TO homestead;

--
-- Name: pricing_rules_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE pricing_rules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pricing_rules_id_seq OWNER TO homestead;

--
-- Name: pricing_rules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE pricing_rules_id_seq OWNED BY pricing_rules.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: homestead; Tablespace: 
--

CREATE TABLE products (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    price numeric(8,2) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    "pricingRuleId" integer NOT NULL,
    "priceType" character varying(255) NOT NULL,
    "pricingRuleParams" json,
    CONSTRAINT "products_priceType_check" CHECK ((("priceType")::text = ANY ((ARRAY['per-item'::character varying, 'per-kg'::character varying])::text[])))
);


ALTER TABLE products OWNER TO homestead;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_id_seq OWNER TO homestead;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY pricing_rules ALTER COLUMN id SET DEFAULT nextval('pricing_rules_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: homestead
--

INSERT INTO migrations (migration, batch) VALUES ('2015_10_12_113728_create_products_table', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_12_121656_create_orders_table', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_12_123030_create_order_items_table', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_12_123451_order_pricing_rule', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_14_101700_drop_orders_entity', 1);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_14_102626_create_pricing_rule_table', 2);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_14_192640_create_relation_from_product_to_pricing_rule', 2);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_14_194819_fix_price_type_column', 3);
INSERT INTO migrations (migration, batch) VALUES ('2015_10_14_201519_add_pricing_rule_params', 4);


--
-- Data for Name: pricing_rules; Type: TABLE DATA; Schema: public; Owner: homestead
--



--
-- Name: pricing_rules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('pricing_rules_id_seq', 1, false);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: homestead
--



--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('products_id_seq', 1, false);


--
-- Name: pricing_rules_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead; Tablespace: 
--

ALTER TABLE ONLY pricing_rules
    ADD CONSTRAINT pricing_rules_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: products_pricingruleid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pricingruleid_foreign FOREIGN KEY ("pricingRuleId") REFERENCES pricing_rules(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

