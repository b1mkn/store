<?php

namespace App\Models;

use App\Models\PricingRule\CustomPriceAfterCustomQuantity;
use App\Models\PricingRule\CustomPriceForCustomQuantity;
use App\Models\PricingRule\FreeItemAfterRequiredQuantity;
use App\Models\PricingRule\RegularPrice;
use App\Models\PricingRule\PricingRuleInterface;
use Illuminate\Support\Facades\Log;

class PricingRule extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];

    /**
     * @return array
     */
    static public function getAvailableRulesList()
    {
        return [
            CustomPriceAfterCustomQuantity::class,
            CustomPriceForCustomQuantity::class,
            FreeItemAfterRequiredQuantity::class,
            RegularPrice::class
        ];
    }

    /**
     * Get the products associated with current pricing rule.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }


    /**
     * Set and validate the name of pricing rule.
     *
     * @param  string  $value
     *
     * @throws \RuntimeException
     * @return string
     */
    public function setNameAttribute($value)
    {
        if (!class_exists($value)) {
            throw new \RuntimeException('Invalid value of `name` attribute');
        }

        if (!((new $value([])) instanceof PricingRuleInterface)) {
            throw new \RuntimeException($value . ' must implement PricingRuleInterface');
        }

        $this->attributes['name'] = $value;
    }
}