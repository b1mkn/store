<?php

namespace App\Models\PricingRule;

use App\Models\Product;

class FreeItemAfterRequiredQuantity implements PricingRuleInterface
{
    protected $requiredQuantity;

    public function __construct(array $params)
    {
        $this->requiredQuantity = array_get($params, 'requiredQuantity');
    }

    /**
     * Apply rule and get total price
     *
     * @param Product $product
     * @param $quantity
     * @return mixed
     */
    public function getPrice(Product $product, $quantity)
    {
        $freeQuantity = floor($quantity / $this->requiredQuantity);

        return $product->price * ($quantity - $freeQuantity);
    }
}