<?php

namespace App\Models\PricingRule;

use App\Models\Product;

interface PricingRuleInterface
{
    public function getPrice(Product $product, $quantity);

    public function __construct(array $params);
}