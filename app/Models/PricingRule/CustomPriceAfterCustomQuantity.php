<?php

namespace App\Models\PricingRule;

use App\Models\Product;

class CustomPriceAfterCustomQuantity implements PricingRuleInterface
{
    protected $customQuantity;

    protected $customPrice = 10;

    public function __construct(array $params)
    {
        $this->customQuantity = array_get($params, 'customQuantity');
        $this->customPrice = array_get($params, 'customPrice');
    }

    /**
     * Apply rule and get total price
     *
     * @param Product $product
     * @param $quantity
     * @return mixed
     */
    public function getPrice(Product $product, $quantity)
    {
        $price = $product->price;

        if ($quantity >= $this->customQuantity) {
            $price = $this->customPrice;
        }

        return $quantity * $price;
    }
}