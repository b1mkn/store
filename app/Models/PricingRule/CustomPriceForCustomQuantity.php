<?php

namespace App\Models\PricingRule;

use App\Models\Product;

class CustomPriceForCustomQuantity implements PricingRuleInterface
{
    protected $customQuantity;

    protected $customPrice = 10;

    public function __construct(array $params)
    {
        $this->customQuantity = array_get($params, 'customQuantity');
        $this->customPrice = array_get($params, 'customPrice');
    }

    /**
     * Apply rule and get total price
     *
     * @param Product $product
     * @param $quantity
     * @return mixed
     */
    public function getPrice(Product $product, $quantity)
    {
        $customQuantity = floor($quantity / $this->customQuantity);
        $totalCustomPrice = $customQuantity * $this->customPrice;

        // amount of products without custom rule
        $remainingQuantity = ($quantity - ($this->customQuantity * $customQuantity));
        $priceWithoutCustom = $product->price * $remainingQuantity;

        return $totalCustomPrice + $priceWithoutCustom;
    }
}