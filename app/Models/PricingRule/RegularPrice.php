<?php

namespace App\Models\PricingRule;

use App\Models\Product;

class RegularPrice implements PricingRuleInterface
{
    protected $requiredQuantity;

    public function __construct(array $params)
    {

    }

    /**
     * Apply rule and get total price
     *
     * @param Product $product
     * @param $quantity
     * @return mixed
     */
    public function getPrice(Product $product, $quantity)
    {
        return $product->price * $quantity;
    }
}