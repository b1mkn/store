<?php

namespace App\Models;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'priceType', 'pricingRule', 'pricingRuleParams'];

    const PRICE_TYPE_PER_ITEM = 'per-item';
    const PRICE_TYPE_PER_KG = 'per-kg';

    /**
     * @return array
     */
    static public function getPriceTypeList()
    {
        return [
            self::PRICE_TYPE_PER_ITEM,
            self::PRICE_TYPE_PER_KG
        ];
    }

    /**
     * Get the pricing rule record associated with the product.
     */
    public function pricingRule()
    {
        return $this->hasOne(PricingRule::class, 'id', 'pricingRuleId');
    }

    /**
     * Return an array of pricing rule parameters
     *
     * @param $value
     * @return mixed
     */
    public function getPricingRuleParamsAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * Set the product's params of pricing rule.
     *
     * @param  array  $value
     * @return string
     */
    public function setPricingRuleParamsAttribute($value)
    {
        $this->attributes['pricingRuleParams'] = json_encode($value);
    }

    public function getPrice($quantity)
    {
        $rule = new $this->pricingRule->name($this->pricingRuleParams);

        return $rule->getPrice($this, $quantity);
    }

}