<?php

namespace App\Tests;

use Faker;
use Illuminate\Foundation;
use Illuminate\Contracts\Console\Kernel;

class TestCase extends Foundation\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();
        return $app;
    }
}