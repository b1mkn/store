<?php

namespace App\Tests;

use App\Models\PricingRule;
use App\Models\Product;
use App\Models\PricingRule\FreeItemAfterRequiredQuantity;
use App\Models\PricingRule\CustomPriceForCustomQuantity;
use App\Models\PricingRule\CustomPriceAfterCustomQuantity;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class ProductsTest extends TestCase
{
    use DatabaseTransactions;

    public function testSuccessfullProductCreate()
    {
        $pricingRule = Factory::create(PricingRule::class, [
            'name' => CustomPriceForCustomQuantity::class
        ]);

        $product = Factory::create(Product::class, [
            'price' => 2.99,
            'priceType' => Product::PRICE_TYPE_PER_ITEM,
            'pricingRuleId' => $pricingRule->id
        ]);

        $this->assertInstanceOf(Product::class, $product);
    }

    /**
     * Expect QueryException because of invalid value in `priceType` column
     */
    public function testInvalidProductCreate()
    {
        $this->setExpectedException(\Illuminate\Database\QueryException::class);

        $pricingRule = Factory::create(PricingRule::class, [
            'name' => CustomPriceForCustomQuantity::class
        ]);

        Factory::create(Product::class, [
            'price' => 2.99,
            'priceType' => 'unknown',
            'pricingRuleId' => $pricingRule->id
        ]);
    }

    public function testPricingRuleParams()
    {
        $pricingRule = Factory::create(PricingRule::class, [
            'name' => CustomPriceForCustomQuantity::class
        ]);

        $product = Factory::create(Product::class, [
            'price' => 2.99,
            'priceType' => Product::PRICE_TYPE_PER_ITEM,
            'pricingRuleId' => $pricingRule->id,
            'pricingRuleParams' => [
                'requiredQuantity' => 3
            ]
        ]);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertArrayHasKey('requiredQuantity', $product->pricingRuleParams);
    }
}