<?php

namespace App\Tests;

use App\Models\PricingRule;
use App\Models\PricingRule\FreeItemAfterRequiredQuantity;
use App\Models\PricingRule\CustomPriceForCustomQuantity;
use App\Models\PricingRule\CustomPriceAfterCustomQuantity;
use App\Models\PricingRule\RegularPrice;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class PricingRuleTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @param $name
     *
     * @dataProvider pricingRules
     */
    public function testSuccessCreateOfExistingPricingRule($name)
    {
        Factory::create(PricingRule::class, [
            'name' => $name
        ]);

        $this->assertEquals(PricingRule::all()->count(), 1);
    }

    public function testCreateOfInvalidPricingRule()
    {
        $this->setExpectedException(\RuntimeException::class);

        Factory::create(PricingRule::class, [
            'name' => 'invalid class name'
        ]);
    }

    public function pricingRules()
    {
        return [
            'CustomPriceForCustomQuantity' => [
                'name' => CustomPriceForCustomQuantity::class
            ],
            'CustomPriceAfterCustomQuantity' => [
                'name' => CustomPriceAfterCustomQuantity::class
            ],
            'FreeItemAfterRequiredQuantity' => [
                'name' => FreeItemAfterRequiredQuantity::class
            ],
            'RegularPrice' => [
                'name' => RegularPrice::class
            ]
        ];
    }
}