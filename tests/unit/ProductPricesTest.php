<?php

namespace App\Tests;

use App\Models\PricingRule;
use App\Models\Product;
use App\Models\PricingRule\FreeItemAfterRequiredQuantity;
use App\Models\PricingRule\CustomPriceForCustomQuantity;
use App\Models\PricingRule\CustomPriceAfterCustomQuantity;
use App\Models\PricingRule\RegularPrice;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laracasts\TestDummy\Factory;

class ProductPricesTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @dataProvider productPrices
     */
    public function testProductPrices($productName, $quantity, $expectedPrice)
    {
        $ruleForRegularPrice = Factory::create(PricingRule::class, ['name' => RegularPrice::class]);
        $ruleForCustomQuantity = Factory::create(PricingRule::class, ['name' => CustomPriceForCustomQuantity::class]);
        $ruleAfterCustomQuantity = Factory::create(PricingRule::class, ['name' => CustomPriceAfterCustomQuantity::class]);
        $ruleForFreeItem = Factory::create(PricingRule::class, ['name' => FreeItemAfterRequiredQuantity::class]);

        $this->products['kiwi'] = Factory::create(Product::class, [
            'name' => 'Kiwi',
            'priceType' => Product::PRICE_TYPE_PER_KG,
            'price' => 22.00,
            'pricingRuleId' => $ruleForRegularPrice->id,
            'pricingRuleParams' => []
        ]);

        $this->products['banana'] = Factory::create(Product::class, [
            'name' => 'Banana',
            'priceType' => Product::PRICE_TYPE_PER_KG,
            'price' => 22.00,
            'pricingRuleId' => $ruleForCustomQuantity->id,
            'pricingRuleParams' => [
                'customQuantity' => 1,
                'customPrice' => 19.99
            ]
        ]);

        $this->products['beer'] = Factory::create(Product::class, [
            'name' => 'Beer',
            'priceType' => Product::PRICE_TYPE_PER_ITEM,
            'price' => 4.00,
            'pricingRuleId' => $ruleForCustomQuantity->id,
            'pricingRuleParams' => [
                'customQuantity' => 3,
                'customPrice' => 10.00
            ]
        ]);

        $this->products['wine'] = Factory::create(Product::class, [
            'name' => 'Wine',
            'priceType' => Product::PRICE_TYPE_PER_ITEM,
            'price' => 50.00,
            'pricingRuleId' => $ruleForFreeItem->id,
            'pricingRuleParams' => [
                'requiredQuantity' => 3
            ]
        ]);

        $this->products['orange'] = Factory::create(Product::class, [
            'name' => 'Orange',
            'priceType' => Product::PRICE_TYPE_PER_KG,
            'price' => 5.00,
            'pricingRuleId' => $ruleAfterCustomQuantity->id,
            'pricingRuleParams' => [
                'customQuantity' => 3,
                'customPrice' => 4.00
            ]
        ]);

        $this->assertEquals(
            $expectedPrice,
            $this->products[$productName]->getPrice($quantity)
        );
    }

    public function productPrices()
    {
        return [
            'half-of-kg-of-kiwis' => [
                'product' => 'kiwi',
                'quantity' => 0.5,
                'expectedPrice' => 11.00
            ],
            '1-kg-of-bananas' => [
                'product' => 'banana',
                'quantity' => 1,
                'expectedPrice' => 19.99
            ],
            '3-bottles-of-beer-for-10' => [
                'product' => 'beer',
                'quantity' => 3,
                'expectedPrice' => 10.00
            ],
            '4-bottles-of-beer-for-14' => [
                'product' => 'beer',
                'quantity' => 4,
                'expectedPrice' => 14.00
            ],
            'buy-two-bottles-of-wine-and-get-third-for-free' => [
                'product' => 'wine',
                'quantity' => 3,
                'expectedPrice' => 100.00
            ],
            'buy-4-bottles-of-wine-for-price-of-3' => [
                'product' => 'wine',
                'quantity' => 4,
                'expectedPrice' => 150.00
            ],
            'oranges-until-required-quantity' => [
                'product' => 'orange',
                'quantity' => 2,
                'expectedPrice' => 10.00
            ],
            'oranges-with-required-quantity' => [
                'product' => 'orange',
                'quantity' => 3,
                'expectedPrice' => 12.00
            ],
        ];
    }
}